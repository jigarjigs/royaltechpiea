package com.royaltechpie.app.royaltechpiebatch.entities;

public class PersonEntity {
    String name;
    int profilePic;

    public PersonEntity(String name, int profilePic) {
        this.name = name;
        this.profilePic = profilePic;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getProfilePic() {
        return profilePic;
    }

    public void setProfilePic(int profilePic) {
        this.profilePic = profilePic;
    }
}
