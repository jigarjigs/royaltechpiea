package com.royaltechpie.app.royaltechpiebatch;

import android.app.Activity;
import android.content.Intent;
import android.content.res.Configuration;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

public class MainActivity extends AppCompatActivity {

    TextView tvTitle;
    ImageView ivBankLogo;
    Button btnSignIn;
    EditText etUsername;
    int count = 0;
    Activity mActivity;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Log.d("ClassName", "MainActivity");
        setContentView(R.layout.activity_main);

        mActivity = this;
        mappingWidgets();
        mappingData();
    }

    private void mappingWidgets() {
        tvTitle = findViewById(R.id.tvTitle);
        ivBankLogo = findViewById(R.id.ivBankLogo);
        btnSignIn = findViewById(R.id.btnSignIn);
        etUsername = findViewById(R.id.etUsername);
    }

    private void mappingData() {
        tvTitle.setText(R.string.app_name);
        ivBankLogo.setImageResource(R.drawable.ic_wallet);
    }

    public void changeMyImage(View view) {
        btnSignIn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                btnSignIn.setBackground(ContextCompat.getDrawable(MainActivity.this, R.drawable.ic_bank));
            }
        });


        count++;
        if (count % 2 == 0) {
            ivBankLogo.setImageResource(R.drawable.ic_bank);
        } else {
            ivBankLogo.setImageResource(R.drawable.ic_wallet);
        }
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
    }

    public void changeMyActivity(View view) {
        //Intent i = new Intent(mActivity,DetailActivity.class);
        Intent i = new Intent("com.royaltechpie.app.royaltechpiebatch.INVOKE_DETAIL");
        i.putExtra("email", etUsername.getText().toString());
        i.putExtra("flag", false);
        i.putExtra("myIntValue", 10);
        //startActivityForResult(i,123);
        startActivity(i);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == 123 && resultCode == RESULT_OK) {
            String str = data.getExtras().getString("combinedString");
            tvTitle.setText(str);
        }
    }

    @Override
    public void onConfigurationChanged(Configuration newConfig) {
        super.onConfigurationChanged(newConfig);
        Log.d("onConfigurationChanged","config change");
        if (newConfig.orientation == Configuration.ORIENTATION_LANDSCAPE) {
            Toast.makeText(mActivity, "LANDSCAPE", Toast.LENGTH_SHORT).show();
        } else if (newConfig.orientation == Configuration.ORIENTATION_PORTRAIT) {
            Toast.makeText(mActivity, "PORTRAIT", Toast.LENGTH_SHORT).show();
        }
    }
}