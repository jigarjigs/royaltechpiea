package com.royaltechpie.app.royaltechpiebatch.adapters;

import android.app.Activity;
import android.content.Intent;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.royaltechpie.app.royaltechpiebatch.MyFragmentFrameActivity;
import com.royaltechpie.app.royaltechpiebatch.R;
import com.royaltechpie.app.royaltechpiebatch.entities.PersonEntity;

import java.util.ArrayList;

public class MyRecyclerAdapter extends RecyclerView.Adapter<MyRecyclerAdapter.MyViewHolder> {

    Activity activity;
    ArrayList<PersonEntity> alPersonEntities;
    public MyRecyclerAdapter(Activity activity){
        this.activity = activity;
        mappingData();

    }

    private void mappingData() {
        alPersonEntities = new ArrayList<>();
        alPersonEntities.add(new PersonEntity("Person Name 1", R.drawable.ic_bank));
        alPersonEntities.add(new PersonEntity("Person Name 2", R.drawable.ic_bank));
        alPersonEntities.add(new PersonEntity("Person Name 3", R.drawable.ic_bank));
        alPersonEntities.add(new PersonEntity("Person Name 4", R.drawable.ic_bank));
        alPersonEntities.add(new PersonEntity("Person Name 5", R.drawable.ic_bank));
        alPersonEntities.add(new PersonEntity("Person Name 6", R.drawable.ic_bank));
        alPersonEntities.add(new PersonEntity("Person Name 7", R.drawable.ic_bank));
        alPersonEntities.add(new PersonEntity("Person Name 8", R.drawable.ic_bank));
        alPersonEntities.add(new PersonEntity("Person Name 9", R.drawable.ic_bank));
        alPersonEntities.add(new PersonEntity("Person Name 10", R.drawable.ic_bank));
        alPersonEntities.add(new PersonEntity("Person Name 11", R.drawable.ic_bank));
        alPersonEntities.add(new PersonEntity("Person Name 12", R.drawable.ic_bank));
        alPersonEntities.add(new PersonEntity("Person Name 13", R.drawable.ic_bank));
        alPersonEntities.add(new PersonEntity("Person Name 14", R.drawable.ic_bank));
        alPersonEntities.add(new PersonEntity("Person Name 15", R.drawable.ic_bank));
    }

    @NonNull
    @Override
    public MyViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        View view = activity.getLayoutInflater().inflate(R.layout.item_my_recycler_view,viewGroup,false);
        return new MyViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull final MyViewHolder myViewHolder, int i) {
        myViewHolder.tvPersonName.setText(alPersonEntities.get(i).getName());
        myViewHolder.ivPersonPic.setImageResource(alPersonEntities.get(i).getProfilePic());
        myViewHolder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                int pos = myViewHolder.getAdapterPosition();
                Toast.makeText(activity,"Position : "+ pos,Toast.LENGTH_SHORT).show();
                Intent i = new Intent(activity, MyFragmentFrameActivity.class);
                activity.startActivity(i);
            }
        });
    }

    @Override
    public int getItemCount() {
        return alPersonEntities.size();
    }

    public class MyViewHolder extends RecyclerView.ViewHolder{

        TextView tvPersonName;
        ImageView ivPersonPic;

        public MyViewHolder(@NonNull View view) {
            super(view);
            tvPersonName = view.findViewById(R.id.tvPersonName);
            ivPersonPic = view.findViewById(R.id.ivPersonPic);
        }
    }

    /**
     * Function to add item in recycler view
     * @param i position of item to be inserted
     * @param personEntity PersonEntity object
     */
    public void addItem(int i, PersonEntity personEntity){
        alPersonEntities.add(i,personEntity);
        notifyItemInserted(i);
    }

    /**
     * Function to remove item from recycler view
     * @param i
     */
    public void removeItem(int i){
        alPersonEntities.remove(i);
        notifyItemRemoved(i);
    }

    public void reArrangeItem(int oldPos,int newPos){
        PersonEntity oldData = alPersonEntities.get(oldPos);
        PersonEntity newData = alPersonEntities.get(newPos);
        alPersonEntities.remove(oldPos);
        alPersonEntities.remove(newPos);
        alPersonEntities.add(oldPos,newData);
        alPersonEntities.add(newPos,oldData);
        notifyItemMoved(oldPos,newPos);
    }

    public void resetAdapter(){
        mappingData();
        notifyDataSetChanged();
    }
}
