package com.royaltechpie.app.royaltechpiebatch;

import android.app.Activity;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.DividerItemDecoration;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.helper.ItemTouchHelper;
import android.view.View;

import com.royaltechpie.app.royaltechpiebatch.adapters.MyRecyclerAdapter;
import com.royaltechpie.app.royaltechpiebatch.entities.PersonEntity;

public class MyRecyclerViewActivity extends AppCompatActivity {

    Activity activity;
    RecyclerView rvPerson;
    MyRecyclerAdapter adapter;
    int i = 16;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_my_recycler_view);
        activity = this;

        mappingWidget(activity);
        mappingData();
    }

    /**
     * Function to Initialize view
     * @param activity Instance of Activity
     */
    private void mappingWidget(Activity activity) {
        rvPerson = findViewById(R.id.rvPerson);
    }

    /**
     * Function to map data with view
     */
    private void mappingData() {
        /*LinearLayoutManager manager = new LinearLayoutManager(activity);
        manager.setOrientation(LinearLayoutManager.VERTICAL);*/
        GridLayoutManager manager = new GridLayoutManager(activity,2);
        rvPerson.setLayoutManager(manager);
        rvPerson.addItemDecoration(new DividerItemDecoration(activity,DividerItemDecoration.VERTICAL));
        adapter = new MyRecyclerAdapter(activity);
        rvPerson.setAdapter(adapter);

        setupRecyclerTouchHelper();
    }

    private void setupRecyclerTouchHelper() {
        ItemTouchHelper.SimpleCallback simpleCallback = new ItemTouchHelper.SimpleCallback(ItemTouchHelper.UP|ItemTouchHelper.DOWN,0) {
            @Override
            public boolean onMove(@NonNull RecyclerView recyclerView, @NonNull RecyclerView.ViewHolder viewHolder, @NonNull RecyclerView.ViewHolder viewHolder1) {
                adapter.reArrangeItem(viewHolder.getAdapterPosition(),viewHolder1.getAdapterPosition());
                return true;
            }

            @Override
            public void onSwiped(@NonNull RecyclerView.ViewHolder viewHolder, int i) {
                adapter.removeItem(viewHolder.getAdapterPosition());
            }
        };
        new ItemTouchHelper(simpleCallback).attachToRecyclerView(rvPerson);
    }

    public void addItem(View view) {
        PersonEntity personEntity = new PersonEntity("Person Name "+i,R.drawable.ic_bank);
        adapter.addItem(3,personEntity);
    }

    public void removeItem(View view) {
        adapter.removeItem(5);
    }

    public void resetItem(View view) {
        adapter.resetAdapter();
    }
}
