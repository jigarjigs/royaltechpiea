package com.royaltechpie.app.royaltechpiebatch.fragments;


import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;

import com.royaltechpie.app.royaltechpiebatch.R;

/**
 * A simple {@link Fragment} subclass.
 */
public class CallFragment extends Fragment {


    public CallFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_call,container,false);
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        mappingWidget(view);
        mappingData();
    }

    private void mappingWidget(View view) {
        TextView tvTitle = view.findViewById(R.id.tvTitle);
        Button btnCall = view.findViewById(R.id.btnCall);
    }

    private void mappingData() {
    }
}
