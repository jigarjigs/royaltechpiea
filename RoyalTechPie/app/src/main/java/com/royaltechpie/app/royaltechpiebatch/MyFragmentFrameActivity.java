package com.royaltechpie.app.royaltechpiebatch;

import android.app.Activity;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v7.app.AppCompatActivity;
import android.view.View;

import com.royaltechpie.app.royaltechpiebatch.fragments.CallFragment;
import com.royaltechpie.app.royaltechpiebatch.fragments.ChatFragment;
import com.royaltechpie.app.royaltechpiebatch.fragments.StatusFragment;

public class MyFragmentFrameActivity extends AppCompatActivity {

    Activity activity;
    FragmentManager fm;
    Fragment CurrentFragment;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_my_fragment_frame);
        activity = this;

        mappingWidget();
        mappingData();
    }

    private void mappingWidget() {
    }

    private void mappingData() {
        fm = getSupportFragmentManager();
        CurrentFragment = new CallFragment();
        fm.beginTransaction().replace(R.id.flContent, CurrentFragment).commit();

    }

    public void replaceFragment(View view) {
        if (view.getId() == R.id.btnCall) {
            if (!(CurrentFragment instanceof CallFragment)) {
                CurrentFragment = new CallFragment();
                fm.beginTransaction().replace(R.id.flContent, CurrentFragment).commit();
            }
        } else if (view.getId() == R.id.btnChat) {
            if(!(CurrentFragment instanceof ChatFragment)) {
                CurrentFragment = new ChatFragment();
                fm.beginTransaction().replace(R.id.flContent, CurrentFragment).commit();
            }
        } else if (view.getId() == R.id.btnStatus) {
            if(!(CurrentFragment instanceof StatusFragment)) {
                CurrentFragment = new StatusFragment();
                fm.beginTransaction().replace(R.id.flContent, CurrentFragment).commit();
            }
        }
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
    }
}
