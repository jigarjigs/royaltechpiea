package com.royaltechpie.app.royaltechpiebatch;

import android.app.Activity;
import android.os.Bundle;
import android.support.v4.app.FragmentManager;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;

import com.royaltechpie.app.royaltechpiebatch.adapters.MyFragmentPagerAdapter;

public class MyFragmentPagerActivity extends AppCompatActivity {

    Activity activity;
    ViewPager vpFragmentPager;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_my_fragment_pager);
        activity = this;

        mappingWidget();
        mappingData();
    }

    private void mappingWidget() {
        vpFragmentPager = findViewById(R.id.vpFragmentPager);
    }

    private void mappingData() {
        FragmentManager fm = getSupportFragmentManager();
        MyFragmentPagerAdapter adapter = new MyFragmentPagerAdapter(fm);
        vpFragmentPager.setAdapter(adapter);
    }
}
