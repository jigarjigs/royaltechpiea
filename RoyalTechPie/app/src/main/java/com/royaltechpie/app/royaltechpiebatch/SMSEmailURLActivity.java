package com.royaltechpie.app.royaltechpiebatch;

import android.Manifest;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.net.Uri;
import android.os.Bundle;
import android.support.v4.app.ActivityCompat;
import android.support.v7.app.AppCompatActivity;
import android.view.View;

public class SMSEmailURLActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_smsemail_url);
    }

    public void sendEmail(View view) {
        Intent i = new Intent(Intent.ACTION_SENDTO);
        i.setData(Uri.parse("mailto:"));
        i.putExtra(Intent.EXTRA_EMAIL, new String[]{"jigar1@gmail.com", "jigar2@gmail.com"});
        i.putExtra(Intent.EXTRA_CC, new String[]{"jigar3@gmail.com", "jigar4@gmail.com"});
        i.putExtra(Intent.EXTRA_BCC, new String[]{"jigar5@gmail.com", "jigar6@gmail.com"});
        i.putExtra(Intent.EXTRA_SUBJECT, "This is subject");
        i.putExtra(Intent.EXTRA_TEXT, "This is some Text");
        if(i.resolveActivity(getPackageManager()) != null) {
            startActivity(Intent.createChooser(i,"Choose from this"));
        }
    }

    public void sendSMS(View view) {
        Intent i = new Intent(Intent.ACTION_SENDTO);
        i.setData(Uri.parse("smsto:+9979667654;+918877887766"));
        i.putExtra("sms_body", "This is SMS body");
        startActivity(i);
    }

    public void makeCall(View view) {
        Intent i = new Intent(Intent.ACTION_DIAL);
        i.setData(Uri.parse("tel:9977887766"));
        if (ActivityCompat.checkSelfPermission(this, Manifest.permission.CALL_PHONE) != PackageManager.PERMISSION_GRANTED) {
            // TODO: Consider calling
            //    ActivityCompat#requestPermissions
            // here to request the missing permissions, and then overriding
            //   public void onRequestPermissionsResult(int requestCode, String[] permissions,
            //                                          int[] grantResults)
            // to handle the case where the user grants the permission. See the documentation
            // for ActivityCompat#requestPermissions for more details.
            return;
        }
        startActivity(i);
    }

    public void redirectToBrowser(View view) {
        Intent i = new Intent(Intent.ACTION_VIEW);
        i.setData(Uri.parse("https://www.google.com"));
        if(i.resolveActivity(getPackageManager()) != null) {
            startActivity(Intent.createChooser(i,"select browser"));
        }
    }
}
