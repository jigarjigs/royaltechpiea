package com.royaltechpie.app.royaltechpiebatch.adapters;

import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentStatePagerAdapter;

import com.royaltechpie.app.royaltechpiebatch.fragments.CallFragment;
import com.royaltechpie.app.royaltechpiebatch.fragments.ChatFragment;
import com.royaltechpie.app.royaltechpiebatch.fragments.StatusFragment;

public class MyFragmentPagerAdapter extends FragmentStatePagerAdapter {

    public MyFragmentPagerAdapter(FragmentManager fm) {
        super(fm);
    }

    @Override
    public Fragment getItem(int i) {
        if(i == 0){
            return new CallFragment();
        } else if (i == 1) {
            return new ChatFragment();
        } else if( i == 2){
            return new StatusFragment();
        }
        return null;
    }

    @Override
    public int getCount() {
        return 3;
    }
}
