package com.royaltechpie.app.royaltechpiebatch;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.widget.TextView;

public class DetailActivity extends AppCompatActivity {

    TextView tvIntentText;
    String str;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main2);

        mappingWidgets();
        mappingData();
        getMyIntent();

    }

    private void getMyIntent() {
        String myEmail = getIntent().getExtras().getString("email");
        boolean myFlag = getIntent().getExtras().getBoolean("flag");
        int myInt = getIntent().getExtras().getInt("myIntValue");
        str = myEmail + myFlag + myInt;
        tvIntentText.setText(str);
    }

    private void mappingWidgets() {
        tvIntentText = findViewById(R.id.tvIntentText);
    }

    private void mappingData() {

    }

    @Override
    public void onBackPressed() {
        //super.onBackPressed();
        Intent i = new Intent(Intent.ACTION_DEFAULT);
        i.putExtra("combinedString",str);
        setResult(RESULT_OK,i);
        finish();
    }
}
